FROM python:3.8
RUN pip install --upgrade pip
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt
COPY templates /usr/src/app/templates/
COPY bmi.py /usr/src/app/
COPY ess.py /usr/src/app/
COPY pss.py /usr/src/app/
COPY phq.py /usr/src/app/
COPY app.py /usr/src/app/
EXPOSE 80
CMD ["python", "/usr/src/app/app.py"]