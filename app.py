from flask import Flask, jsonify, request, render_template
import bmi,phq,pss,ess


app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/api')
def api():
    return render_template('api.html')

@app.route('/usage')
def usage():
    return render_template('usage.html')

@app.route('/health')
def evaluate():
    res = request.args.to_dict()
    weight = float(res['weight'])
    height = float(res['height'])
    bmi_val = bmi.get_bmi(weight,height)
    bmi_remark = bmi.bmi_eval(bmi_val)

    phq_arr = res['phq'].split(',')
    phq_list = [int(i) for i in phq_arr]
    phq_remark = phq.phq_param(phq_list)

    ess_arr = res['ess'].split(',')
    ess_list = [int(i) for i in ess_arr]
    ess_remark = ess.ess_param(ess_list)

    pss_arr = res['pss'].split(',')
    pss_list = [int(i) for i in pss_arr]
    pss_remark = pss.pss_param(pss_list)

    response_dict={
        'bmi':bmi_remark,
        'phq':phq_remark,
        'ess':ess_remark,
        'pss':pss_remark
    }
    return (jsonify(response_dict))

if __name__=="__main__":
    app.run(host="0.0.0.0",port=80)