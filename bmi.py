#to calculate BMI

def get_bmi(weight,height):
    if(height<=0 or weight<=0):
        return 0
    else:   
        height_sq = height*height
        value = weight/height_sq

    return round(value,1)


def bmi_eval(value):
    if value<=18.5:
        return "Underweight"
    elif value>18.5 and value<=24.9:
        return "Healthy"
    elif value>24.9 and value<=29.9:
        return "Overweight"
    elif value>29.9:
        return "Obese"