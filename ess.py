#calculation for Epworth Sleepiness scale

def ess_param(arr):
    total = sum(arr)

    if total<=6:
        return "Normal"
    elif total>6 and total<9:
        return "Average"
    elif total>=9 and total<=24:
        return "Abnormal"
    else:
        return "Error"