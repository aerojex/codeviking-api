import requests,sys
ip_addr = "3.108.40.52:8181"
test_cases = [
    "http://"+ip_addr+"/health?weight=65&height=1.7&pss=0,0,0,0,0,0,0&ess=0,0,0,0,0,0,0,0&phq=0,0,0,0,0,0,0,0,0",
    "http://"+ip_addr+"/health?weight=10&height=10&pss=1,1,1,1,1,1,1&ess=1,1,1,1,1,1,1,1&phq=1,1,1,1,1,1,1,1,1",
    "http://"+ip_addr+"/health?weight=100&height=2&pss=2,2,2,2,2,2,2&ess=2,2,2,2,2,2,2,2&phq=2,2,2,2,2,2,2,2,2",
    "http://"+ip_addr+"/health?weight=200&height=2&pss=3,3,3,3,3,3,3&ess=3,3,3,3,3,3,3,3&phq=3,3,3,3,3,3,3,3,3"

]

expected_output = [
    {'bmi':"Healthy",'ess':"Normal",'phq':"None",'pss':"Mild"},
    {'bmi':"Underweight",'ess':"Average",'phq':"Mild",'pss':"Mild"},
    {"bmi":"Overweight","ess":"Abnormal","phq":"Moderately Severe","pss":"Moderate"},
    {"bmi":"Obese","ess":"Abnormal","phq":"Severe","pss":"Severe"}
]

for i in range(0,len(test_cases)):
    try:
        uri = test_cases[i]
        ans = expected_output[i]
        response = requests.get(uri)
        res = response.json()
        assert(res['bmi']==ans['bmi'] and res['ess']==ans['ess'] and res['phq']==ans['phq'] and res['pss']==ans['pss'])
        print("TEST ", i+1 ," PASSED")
    except Exception as e:
        print("TEST ",i+1," FAILED!")
        sys.exit(-1)
