import bmi,phq,pss,ess
import unittest


class test_case(unittest.TestCase):

    def test_case_bmi(self):
        self.assertEqual(bmi.get_bmi(69,1.7),23.9,"Should be 23.9")
        self.assertEqual(bmi.get_bmi(100,0),0,"Should be 0")
        self.assertEqual(bmi.get_bmi(0,100),0,"Should be 0")
        self.assertEqual(bmi.get_bmi(0,0),0,"Should be 0")
        self.assertEqual(bmi.get_bmi(100,1),100,"Should be 100")
        self.assertEqual(bmi.get_bmi(-10,0),0,"Should be 0")
        self.assertEqual(bmi.get_bmi(0,-1),0,"Should be 0")
        self.assertEqual(bmi.get_bmi(-1,-1),0,"Should be 0")
    
    def test_case_pss(self):

        tc1 = [1,2,3,4,5,5,5]
        tc2 = [0,0,0,0,0,0,0]
        tc3 = [-1,-1,-2,3,-4,0,4]
        self.assertEqual(pss.pss_param(tc1),"Severe","Should be Severe")
        self.assertEqual(pss.pss_param(tc2),"Mild","Should be Mild")
        self.assertEqual(pss.pss_param(tc3),"Error","Should be Error")


if __name__=='__main__':
    unittest.main()